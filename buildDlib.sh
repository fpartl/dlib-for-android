#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Chyba: Skript očekává 1 parametr -- ABI pro které má být knihovna sestavena (armeabi-v7a | x86)."
    exit 1
fi

if [ -z "$ANDROID_PLATFORM" ]; then
    echo "error: undefined NDK_PLATFORM_LEVEL"
    exit 1
fi

################################################################
### Konfigurační konstanty #####################################
################################################################

# Umístení NDK
NDK_PATH="$HOME/Programming/AndroidNDK/android-ndk-r18b"
DLIB_VERSION=19.17
ANDROID_PLATFORM=21
ANDROID_SDK="$HOME/Programming/AndroidSDK"

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# Adresář, kam se budou stahovat zdrojové soubory knihovny.
SOURCE_DIR=$CURRENT_DIR/sources

# Adresář, kam se uloží sestavené knihovny.
BUILD_DIR=$CURRENT_DIR/build/dlib-$DLIB_VERSION/android-$1

# Aktální adresář.
BUILDER_HOME=$(pwd)

###################################################################
### Odstraňování předchozích instancí sestavování #################
###################################################################
echo "Odstraňuji všechny zdrojové i binární soubory..."
rm -rf $SOURCE_DIR
mkdir -p $SOURCE_DIR

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

################################################################
### Sestavování knihovny #######################################
################################################################
cd $SOURCE_DIR

if [ ! -d "dlib-$DLIB_VERSION" ]; then
    git clone https://github.com/davisking/dlib.git dlib-$DLIB_VERSION
fi

cd dlib-$DLIB_VERSION
git checkout tags/v$DLIB_VERSION
cd dlib

DLIB_SOURCE=$(pwd)


cd $BUILD_DIR
git clean -d -f -x

export CMAKE=$ANDROID_SDK/cmake/3.10.2.4988404/bin/cmake
export NINJA=$ANDROID_SDK/cmake/3.10.2.4988404/bin/ninja

$CMAKE -GNinja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_TOOLCHAIN_FILE=$NDK_PATH/build/cmake/android.toolchain.cmake \
    -DCMAKE_INSTALL_PREFIX=$BUILD_DIR \
    -DANDROID_NDK=$NDK_PATH \
    -DANDROID_TOOLCHAIN=clang \
    -DCMAKE_ANDROID_ARCH_ABI=$1 \
    -DANDROID_ABI=$1 \
    -DANDROID_LINKER_FLAGS="-landroid -llog" \
    -DANDROID_NATIVE_API_LEVEL=$ANDROID_PLATFORM \
    -DANDROID_STL=c++_static \
    -DDLIB_PNG_SUPPORT=ON \
    -DDLIB_JPEG_SUPPORT=ON \
    -DDLIB_NO_GUI_SUPPORT=TRUE \
    -DDLIB_USE_BLAS=FALSE \
    -DDLIB_USE_LAPACK=FALSE \
    -DANDROID_CPP_FEATURES="rtti exceptions" \
    -DCMAKE_MAKE_PROGRAM=$NINJA \
    -DBUILD_SHARED_LIBS=1 \
    $DLIB_SOURCE
    
$NINJA
$NINJA install

rm -rf $SOURCE_DIR